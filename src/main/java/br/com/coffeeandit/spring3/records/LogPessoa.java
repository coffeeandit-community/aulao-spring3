package br.com.coffeeandit.spring3.records;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@Slf4j
public class LogPessoa {

    public static final int BOUND = 3;

    public void log(final Object pessoa) {
        int randon = new Random().nextInt(BOUND);
        switch (randon) {
            case 0:
                printObject(pessoa);
            case 1:
                printRecords(pessoa);
            default:
                printSwitch(pessoa);
        }

    }

    private void printObject(final Object pessoa) {
        if (pessoa instanceof PessoaFisica pessoaFisica) {
            print(pessoaFisica.nome(), pessoaFisica.documento(), pessoaFisica.endereco());
        } else if (pessoa instanceof PessoaJuridica pessoaJuridica) {
            print(pessoaJuridica.nome(), pessoaJuridica.documento(), pessoaJuridica.endereco());
        } else if (pessoa instanceof PessoaTerceiro pessoaTerceiro) {
            print(pessoaTerceiro.nome(), pessoaTerceiro.documento(), pessoaTerceiro.endereco());
        }
    }

    private void printRecords(final Object pessoa) {
        if (pessoa instanceof PessoaFisica(String nome, long cpf, Endereco endereco)) {
            print(nome, cpf, endereco);
        } else if (pessoa instanceof PessoaJuridica(String nome, long cnpj, Endereco endereco)) {
            print(nome, cnpj, endereco);
        } else if (pessoa instanceof PessoaTerceiro pessoaTerceiro) {
            print(pessoaTerceiro.nome(), pessoaTerceiro.documento(), pessoaTerceiro.endereco());
        }
    }

    private void printSwitch(final Object pessoa) {
        // Pattern Matching
        switch (pessoa) {
            case PessoaFisica(String nome, long cpf, Endereco endereco) -> print(nome, cpf, endereco);
            case PessoaJuridica(String nome, long cnpj, Endereco endereco) -> print(nome, cnpj, endereco);
            case PessoaTerceiro pessoaTerceiro ->
                    print(pessoaTerceiro.nome(), pessoaTerceiro.documento(), pessoaTerceiro.endereco());
            default -> throw new IllegalStateException("Unexpected value: " + pessoa);
        }
    }

    private void print(final String nome, long documento, final Endereco endereco) {
        log.info("A pessoa de " + nome + " e de " + documento + " domicialiada no endereço: " + endereco.rua() + " de número: " + endereco.numero() + " \n foi pesquisada na base de dados.");
    }
}
