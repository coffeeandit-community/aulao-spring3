package br.com.coffeeandit.spring3.repository;


import br.com.coffeeandit.spring3.records.Endereco;
import br.com.coffeeandit.spring3.records.LogPessoa;
import br.com.coffeeandit.spring3.records.PessoaFisica;
import br.com.coffeeandit.spring3.records.PessoaJuridica;
import io.micrometer.observation.annotation.Observed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
@Observed(name = "pessoaRepository")
@Slf4j
public class PessoaRepository {

    private static final int BOUND = 5;
    private static final int ZERO = 0;

    public PessoaRepository(LogPessoa logPessoa) {
        this.logPessoa = logPessoa;
    }

    private final LogPessoa logPessoa;

    public PessoaJuridica findByCNPJ(final Long cnpj) {
        PessoaJuridica pessoaJuridica = null;
        try {
        log.info("Buscando Pessoa Juridica pelo CPNJ {}", cnpj);
            pessoaJuridica = new PessoaJuridica(randomIdentifier(), cnpj, randonAddress());
            return pessoaJuridica;

        } finally {
            logPessoa.log(pessoaJuridica);

        }
    }

    public PessoaFisica findByCPF(final Long cpf) {
        PessoaFisica pessoaFisica = null;
        try {
            log.info("Buscando Pessoa Física pelo CPNJ {}", cpf);
            pessoaFisica = new PessoaFisica(randomIdentifier(), cpf, randonAddress());
            return pessoaFisica;

        } finally {
            logPessoa.log(pessoaFisica);

        }
    }

    private final static String LEXICON = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";

    private final java.util.Random random = new java.util.Random();

    private final Set<String> identifiers = new HashSet<String>();

    private String randomIdentifier() {
        var builder = new StringBuilder();
        while (builder.toString().length() == ZERO) {
            int length = random.nextInt(BOUND) + BOUND;
            for (int i = ZERO; i < length; i++) {
                builder.append(LEXICON.charAt(random.nextInt(LEXICON.length())));
            }
            if (identifiers.contains(builder.toString())) {
                builder = new StringBuilder();
            }
        }
        return builder.toString();
    }

    private Endereco randonAddress() {
        return new Endereco(randomIdentifier(), random.nextInt(BOUND));
    }
}
