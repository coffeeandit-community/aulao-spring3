package br.com.coffeeandit.spring3.api;


import br.com.coffeeandit.spring3.records.PessoaFisica;
import br.com.coffeeandit.spring3.records.PessoaJuridica;
import br.com.coffeeandit.spring3.repository.PessoaRepository;
import jakarta.validation.ConstraintViolationException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
public class PessoaAPI {

    private PessoaRepository pessoaRepository;

    @GetMapping(value = "/pessoa/juridica/{cnpj}")
    public PessoaJuridica findByCNPJ(@PathVariable("cnpj") Long cnpj) {
        return pessoaRepository.findByCNPJ(cnpj);
    }

    @GetMapping(value = "/pessoa/fisica/{cpf}")
    public PessoaFisica findByCPF(@PathVariable("cpf") Long cpf) {
        return pessoaRepository.findByCPF(cpf);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
        ResponseEntity<String> handleConstraintViolationException(final ConstraintViolationException e) {
        return new ResponseEntity<>("erro ao validar atributo: " + e.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
