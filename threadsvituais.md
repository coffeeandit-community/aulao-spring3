Finalmente estamos colhendo os primeiros resultados do Project Loom, trazendo Virtual Threads para a JVM. Virtual threads são threas controlados pelo Java Runtime, em oposição aos Platform Threads que dependem de threads do sistema operacional.

Threads de runtime Java geralmente são correspondentes à threads do kernel do sistema (one-to-one), e o thread scheduler do kernel do sistema é responsável pelo agendamento das threads Java.

<img src="https://ivanmarreta172605709.files.wordpress.com/2022/11/image.png">

O modelo acima era até então o modelo multithreading que utilizamos para resolver problemas de programação concorrente. Mas temos dois problemas centrais com as threads da máquina: são caras e têm um número limitado.
Sendo threads do sistema operacional, elas são caras para criar, tanto em tempo quanto em consumo de recursos. É por isso que você só pode ter tantos deles antes de ficar sem recursos. E quando uma plataform thread é bloqueada, a thread do sistema operacional também é bloqueada, sendo assim nenhum outro código pode ser executado na thread do sistema operacional durante o período de bloqueio.

Virtual threads chegaram para resolver esse problema. A thread que usamos até agora é chamada de platform thread que ainda corresponde à thread do kernel do sistema (aquela one-to-one). Um grande número (M) de virtual threads é executado em um número menor (N) de platform threads (programação M:N).

<img src="https://ivanmarreta172605709.files.wordpress.com/2022/11/image-1.png">

Multiplos virtual threads serão scheduled pela JVM para execução em um determinado encadeamento de plataforma e um platform thread executará apenas uma virtual thread ao mesmo tempo.

Ou seja, o Java 19 apresenta uma prévia de uma alternativa que deve melhorar drasticamente o manuseio de operações simultâneas por servidores.