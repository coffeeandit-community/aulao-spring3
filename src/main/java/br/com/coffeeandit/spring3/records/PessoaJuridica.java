package br.com.coffeeandit.spring3.records;

public record PessoaJuridica(String nome, long documento, Endereco endereco) implements Pessoa {
}
