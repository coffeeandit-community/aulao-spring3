package br.com.coffeeandit.spring3.records;

public sealed interface Pessoa permits PessoaFisica, PessoaJuridica, PessoaTerceiro {

    Endereco endereco();

    String nome();

    public default String tipoPessoa() {
        return getClass().getSimpleName();
    }
}
