package br.com.coffeeandit.spring3.records;

public non-sealed class PessoaTerceiro implements Pessoa {

    private final String nome;
    private final Endereco endereco;
    private Long documento;

    public PessoaTerceiro(final String nome, final Long documento, final Endereco endereco) {
        this.nome = nome;
        this.endereco = endereco;
        this.documento = documento;
    }


    @Override
    public Endereco endereco() {
        return endereco;
    }

    public Long documento() {
        return this.documento;
    }

    @Override
    public String nome() {
        return nome;
    }
}
