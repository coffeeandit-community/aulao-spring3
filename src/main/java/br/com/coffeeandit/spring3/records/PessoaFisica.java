package br.com.coffeeandit.spring3.records;

public record PessoaFisica(String nome, long documento, Endereco endereco) implements Pessoa {
}
