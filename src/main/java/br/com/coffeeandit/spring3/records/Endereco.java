package br.com.coffeeandit.spring3.records;
public record Endereco(String rua, long numero) {
}
