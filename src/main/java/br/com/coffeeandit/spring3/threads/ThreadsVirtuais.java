package br.com.coffeeandit.spring3.threads;

import java.time.Duration;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class ThreadsVirtuais {
    public static void main(String[] args) {

        long currentTimeMillis = System.currentTimeMillis();

        try (var executor = Executors.newFixedThreadPool(8)) {
            IntStream.range(0, 1000).forEach(intStream -> {
                executor.submit(() -> {
                    Thread.sleep(Duration.ofMillis(100));
                    System.out.println("newFixedThreadPool: " + intStream);
                    return intStream;
                });
            });
            executor.shutdown();
        }
        System.out.printf("Tempo de execução newFixedThreadPool: %dms\n", System.currentTimeMillis() - currentTimeMillis);

        currentTimeMillis = System.currentTimeMillis();

        try (var executor = Executors.newVirtualThreadPerTaskExecutor()) {
            IntStream.range(0, 1000).forEach(intStream -> {
                executor.submit(() -> {
                    Thread.sleep(Duration.ofMillis(100));
                    System.out.println("newVirtualThreadPerTaskExecutor: " + intStream);
                    return intStream;
                });
            });
            executor.shutdown();
        }

        System.out.printf("Tempo de execução newVirtualThreadPerTaskExecutor: %dms\n", System.currentTimeMillis() - currentTimeMillis);

        currentTimeMillis = System.currentTimeMillis();

        Thread.ofVirtual().name("virtualThread").start(() -> {
            System.out.println("Uma thread virtual ? "  + Thread.currentThread().isVirtual());
        });
        Thread.ofPlatform().name("virtualPlatform").start(() -> {
            System.out.println("Uma thread de plataforma virtual ? "  + Thread.currentThread().isVirtual());
        });

    }


}